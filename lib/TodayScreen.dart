import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:login_ui_design/login_screen.dart';
import 'package:slide_to_act/slide_to_act.dart';

class TodayScreen extends StatefulWidget {
  const TodayScreen({key}) : super(key: key);
  @override
  _TodayScreenState createState() => _TodayScreenState();
}

class _TodayScreenState extends State<TodayScreen> {
  double screenHeight = 0;
  double screenWidth = 0;

  String checkIn = "--/--";
  String checkOut = "--/--";
  String location = " ";
  String scanResult = " ";
  String officeCode = " ";

  Color primary = Colors.blueAccent;

  @override
  void initState() {
    super.initState();
    _getRecord();
  }

  Future<void> scanQRandCheck() async {
    String result = " ";

    try {} catch (e) {
      print("error");
    }

    if (scanResult == officeCode) {
      QuerySnapshot snap = await FirebaseFirestore.instance
          .collection("user")
          .where(FirebaseAuth.instance.currentUser.email)
          .get();

      DocumentSnapshot snap2 = await FirebaseFirestore.instance
          .collection("user")
          .doc(FirebaseAuth.instance.currentUser.email)
          .collection("Record")
          .doc(DateFormat('dd MMM yyyy').format(DateTime.now()))
          .get();

      try {
        String checkIn = snap2['checkIn'];

        setState(() {
          checkOut = DateFormat('HH:mm').format(DateTime.now());
        });

        await FirebaseFirestore.instance
            .collection("user")
            .doc(FirebaseAuth.instance.currentUser.email)
            .collection("Record")
            .doc(DateFormat('dd MMM yyyy').format(DateTime.now()))
            .update({
          'date': Timestamp.now(),
          'checkIn': checkIn,
          'checkOut': DateFormat('HH:mm').format(DateTime.now()),
          'checkInLocation': location,
        });
      } catch (e) {
        setState(() {
          checkIn = DateFormat('HH:mm').format(DateTime.now());
        });

        await FirebaseFirestore.instance
            .collection("user")
            .doc(FirebaseAuth.instance.currentUser.email)
            .collection("Record")
            .doc(DateFormat('dd MMM yyyy').format(DateTime.now()))
            .set({
          'date': Timestamp.now(),
          'checkIn': DateFormat('HH:mm').format(DateTime.now()),
          'checkOut': "--/--",
          'checkOutLocation': location,
        });
      }
    }
  }

  void _getRecord() async {
    try {
      QuerySnapshot snap = await FirebaseFirestore.instance
          .collection("user")
          .where(FirebaseAuth.instance.currentUser.email)
          .get();

      DocumentSnapshot snap2 = await FirebaseFirestore.instance
          .collection("user")
          .doc(FirebaseAuth.instance.currentUser.email)
          .collection("Record")
          .doc(DateFormat('dd MMM yyyy').format(DateTime.now()))
          .get();

      setState(() {
        checkIn = snap2['checkIn'] ?? '--/--';
        checkOut = snap2['checkOut'] ?? '--/--';
      });
    } catch (e) {
      setState(() {
        checkIn = "--/--";
        checkOut = "--/--";
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;
    screenWidth = MediaQuery.of(context).size.width;

    return Scaffold(
        body: SingleChildScrollView(
      padding: const EdgeInsets.all(20),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 32),
            child: Text(
              "Welcome,",
              style: TextStyle(
                color: Colors.black54,
                fontFamily: "NexaRegular",
                fontSize: screenWidth / 20,
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            child: Text(
              FirebaseAuth.instance.currentUser.email,
              style: TextStyle(
                fontFamily: "NexaBold",
                fontSize: screenWidth / 18,
              ),
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(top: 32),
            child: Text(
              "Today's Status",
              style: TextStyle(
                fontFamily: "NexaBold",
                fontSize: screenWidth / 18,
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 12, bottom: 32),
            height: 150,
            decoration: const BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 10,
                  offset: Offset(2, 2),
                ),
              ],
              borderRadius: BorderRadius.all(Radius.circular(20)),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Check In",
                        style: TextStyle(
                          fontFamily: "NexaRegular",
                          fontSize: screenWidth / 20,
                          color: Colors.black54,
                        ),
                      ),
                      Text(
                        checkIn,
                        style: TextStyle(
                          fontFamily: "NexaBold",
                          fontSize: screenWidth / 18,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Check Out",
                        style: TextStyle(
                          fontFamily: "NexaRegular",
                          fontSize: screenWidth / 20,
                          color: Colors.black54,
                        ),
                      ),
                      Text(
                        checkOut,
                        style: TextStyle(
                          fontFamily: "NexaBold",
                          fontSize: screenWidth / 18,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
              alignment: Alignment.centerLeft,
              child: RichText(
                text: TextSpan(
                  text: DateTime.now().day.toString(),
                  style: TextStyle(
                    color: primary,
                    fontSize: screenWidth / 18,
                    fontFamily: "NexaBold",
                  ),
                  children: [
                    TextSpan(
                      text: DateFormat(' MMM yyyy').format(DateTime.now()),
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: screenWidth / 20,
                        fontFamily: "NexaBold",
                      ),
                    ),
                  ],
                ),
              )),
          StreamBuilder(
            stream: Stream.periodic(const Duration(seconds: 1)),
            builder: (context, snapshot) {
              return Container(
                alignment: Alignment.centerLeft,
                child: Text(
                  DateFormat('HH:mm:ss a').format(DateTime.now()),
                  style: TextStyle(
                    fontFamily: "NexaRegular",
                    fontSize: screenWidth / 20,
                    color: Colors.black54,
                  ),
                ),
              );
            },
          ),
          checkOut == "--/--"
              ? Container(
                  margin: const EdgeInsets.only(top: 24, bottom: 12),
                  child: Builder(
                    builder: (context) {
                      final GlobalKey<SlideActionState> key = GlobalKey();

                      return SlideAction(
                        text: checkIn == "--/--"
                            ? "Slide to Check In"
                            : "Slide to Check Out",
                        textStyle: TextStyle(
                          color: Colors.black54,
                          fontSize: screenWidth / 20,
                          fontFamily: "NexaRegular",
                        ),
                        outerColor: Colors.white,
                        innerColor: primary,
                        key: key,
                        onSubmit: () async {
                          QuerySnapshot snap = await FirebaseFirestore.instance
                              .collection("user")
                              .where('id',
                                  isEqualTo:
                                      FirebaseAuth.instance.currentUser.email)
                              .get();

                          DocumentSnapshot snap2 = await FirebaseFirestore
                              .instance
                              .collection("user")
                              .doc(FirebaseAuth.instance.currentUser.email)
                              .collection("Record")
                              .doc(DateFormat('dd MMM yyyy')
                                  .format(DateTime.now()))
                              .get();

                          try {
                            String checkIn = snap2['checkIn'];

                            setState(() {
                              checkOut =
                                  DateFormat('HH:mm').format(DateTime.now());
                            });

                            await FirebaseFirestore.instance
                                .collection("user")
                                .doc(FirebaseAuth.instance.currentUser.email)
                                .collection("Record")
                                .doc(DateFormat('dd MMM yyyy')
                                    .format(DateTime.now()))
                                .update({
                              'date': Timestamp.now(),
                              'checkIn': checkIn,
                              'checkOut':
                                  DateFormat('HH:mm').format(DateTime.now()),
                              'checkInLocation': location,
                            });
                          } catch (e) {
                            setState(() {
                              checkIn =
                                  DateFormat('HH:mm').format(DateTime.now());
                            });

                            await FirebaseFirestore.instance
                                .collection("user")
                                .doc(FirebaseAuth.instance.currentUser.email)
                                .collection("Record")
                                .doc(DateFormat('dd MMM yyyy')
                                    .format(DateTime.now()))
                                .set({
                              'date': Timestamp.now(),
                              'checkIn':
                                  DateFormat('HH:mm').format(DateTime.now()),
                              'checkOut': "--/--",
                              'checkOutLocation': location,
                            });
                          }

                          try {
                            key.currentState.reset();
                          } catch (e) {
                            //
                          }
                        },
                      );
                    },
                  ),
                )
              : Container(
                  margin: const EdgeInsets.only(top: 32, bottom: 32),
                  child: Text(
                    "You have completed this day!",
                    style: TextStyle(
                      fontFamily: "NexaRegular",
                      fontSize: screenWidth / 20,
                      color: Colors.black54,
                    ),
                  ),
                ),
          location != " "
              ? Text(
                  "Location: " + location,
                )
              : const SizedBox(),
        ],
      ),
    ));
  }
}

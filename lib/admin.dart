import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:intl/intl.dart';
import 'package:login_ui_design/login_screen.dart';
import 'firebase_options.dart';

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List dataUser = [];

  @override
  void initState() {
    super.initState();
    getData();
  }

  void signUserOut() {
    FirebaseAuth.instance.signOut();
  }

  getData() async {
    var user = await FirebaseFirestore.instance.collection('user').get();
    var tanggalSekarang = DateFormat('dd MMM yyyy').format(DateTime.now());

    setState(() {
      dataUser = [];
    });

    await user.docs.forEach((element) async {
      var itemUser = element.data();
      var record = await element.reference
          .collection('Record')
          .doc(tanggalSekarang)
          .get();

      try {
        itemUser['checkIn'] = record.data()['checkIn'] ?? '--/--';
        itemUser['checkOut'] = record.data()['checkOut'] ?? '--/--';
      } catch (e) {
        itemUser['checkIn'] = '--/--';
        itemUser['checkOut'] = '--/--';
      }

      setState(() {
        dataUser.add(itemUser);
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Data Absen'),
        actions: [
          IconButton(
            onPressed: (() => LoginScreen()),
            icon: Icon(Icons.logout),
          )
        ],
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          var item = dataUser[index];

          return Column(
            children: [
              Card(
                child: ListTile(
                  title: Text(item['nama']),
                  subtitle: Row(
                    children: [
                      Text('Check In : ${item['checkIn']}'),
                      Text('     '),
                      Text('Check Out : ${item['checkOut']}'),
                    ],
                  ),
                  // trailing: const Icon(Icons.arrow_forward_ios),
                ),
              ),
            ],
          );
        },
        itemCount: dataUser.length,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getData,
        child: Icon(Icons.refresh),
      ),
    );
  }
}

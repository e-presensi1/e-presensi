// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyA_aD4rf8PKYz2sjbGYC0ohKZvXShde1_I',
    appId: '1:569142150663:web:76dccf7a35037caa828ca2',
    messagingSenderId: '569142150663',
    projectId: 'inviteqr-e34f8',
    authDomain: 'inviteqr-e34f8.firebaseapp.com',
    storageBucket: 'inviteqr-e34f8.appspot.com',
    measurementId: 'G-PVZTWZZ6WF',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyB0SgmZz5vB-fL8RIyl6C0uyObiDheKNYU',
    appId: '1:569142150663:android:6b9383d08ef37b2f828ca2',
    messagingSenderId: '569142150663',
    projectId: 'inviteqr-e34f8',
    storageBucket: 'inviteqr-e34f8.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyDeffbcY6BZj0eFMGHC5F_N9X9h27pad7Q',
    appId: '1:569142150663:ios:052cee9cd282ae30828ca2',
    messagingSenderId: '569142150663',
    projectId: 'inviteqr-e34f8',
    storageBucket: 'inviteqr-e34f8.appspot.com',
    iosClientId: '569142150663-ifpqu6hve681hgvsj4okbgjs4h05dup5.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterApplication1',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyDeffbcY6BZj0eFMGHC5F_N9X9h27pad7Q',
    appId: '1:569142150663:ios:052cee9cd282ae30828ca2',
    messagingSenderId: '569142150663',
    projectId: 'inviteqr-e34f8',
    storageBucket: 'inviteqr-e34f8.appspot.com',
    iosClientId: '569142150663-ifpqu6hve681hgvsj4okbgjs4h05dup5.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterApplication1',
  );
}
